REM make expected staff page 3 
set pdftarget="C:\Files\Pineapples\Projects\Kemis\Form\PRI\out\KEMIS PRI CA3 expected staff.pdf"
set pdfincr=15

"C:\Files\VSProjects\Softwords.Pdf.Tools\Softwords.Pdf.Tools\bin\Release\cenoPostProcess.exe" --tool "duppage" --files "KEMIS PRI CA2 expected staff.pdf" --fieldmask "TL.##.*" --segment 1 --increment %pdfincr% --output %pdftarget%


set pdftarget="C:\Files\Pineapples\Projects\Kemis\Form\PRI\out\KEMIS PRI CA4 expected staff.pdf"
set pdfincr=30

"C:\Files\VSProjects\Softwords.Pdf.Tools\Softwords.Pdf.Tools\bin\Release\cenoPostProcess.exe" --tool "duppage" --files "KEMIS PRI CA2 expected staff.pdf" --fieldmask "TL.##.*" --segment 1 --increment %pdfincr% --output %pdftarget%


REM make classrooms page 2
set pdftarget="C:\Files\Pineapples\Projects\Kemis\Form\PRI\out\KEMIS PRI D02 extra classrooms.pdf"
set pdfincr=10

"C:\Files\VSProjects\Softwords.Pdf.Tools\Softwords.Pdf.Tools\bin\Release\cenoPostProcess.exe" --tool "duppage" --files "C:\Files\Pineapples\Projects\Kemis\Form\PRI\out\KEMIS MASTER PRI extra classrooms.pdf" --fieldmask "Room.Class.##.*" --segment 2 --increment %pdfincr% --output "%pdftarget%"


REM furniture
"C:\Files\VSProjects\Softwords.Pdf.Tools\Softwords.Pdf.Tools\bin\Release\cenoPostProcess.exe" --tool "duppage" --files "%pdftarget%" --fieldmask "Room.Class.F.D.*.## Room.Class.FT.D.*.## Room.Class.FS.D.*.##" --segment 5 --increment %pdfincr% --output %pdftarget%


REM condition
"C:\Files\VSProjects\Softwords.Pdf.Tools\Softwords.Pdf.Tools\bin\Release\cenoPostProcess.exe" --tool "duppage" --files %pdftarget% --fieldmask "Room.Class.*.##.C Room.Class.No.##" --segment 3 --increment %pdfincr% --output %pdftarget%

REM Class Number column headers

"C:\Files\VSProjects\Softwords.Pdf.Tools\Softwords.Pdf.Tools\bin\Release\cenoPostProcess.exe" --tool "setcounters" --files %pdftarget% --fieldmask "Room.Class.No.##" --segment 3 --increment 1 --output %pdftarget%



REM make classrooms page 3

set pdftarget="C:\Files\Pineapples\Projects\Kemis\Form\PRI\out\KEMIS PRI D03 extra classrooms.pdf"
set pdfincr=20

"C:\Files\VSProjects\Softwords.Pdf.Tools\Softwords.Pdf.Tools\bin\Release\cenoPostProcess.exe" --tool "duppage" --files "C:\Files\Pineapples\Projects\Kemis\Form\PRI\out\KEMIS MASTER PRI extra classrooms.pdf" --fieldmask "Room.Class.##.*" --segment 2 --increment %pdfincr% --output %pdftarget%


REM furniture
"C:\Files\VSProjects\Softwords.Pdf.Tools\Softwords.Pdf.Tools\bin\Release\cenoPostProcess.exe" --tool "duppage" --files %pdftarget% --fieldmask "Room.Class.F.D.*.## Room.Class.FT.D.*.## Room.Class.FS.D.*.##" --segment 5 --increment %pdfincr% --output %pdftarget%


REM condition
"C:\Files\VSProjects\Softwords.Pdf.Tools\Softwords.Pdf.Tools\bin\Release\cenoPostProcess.exe" --tool "duppage" --files %pdftarget% --fieldmask "Room.Class.*.##.C Room.Class.No.##" --segment 3 --increment %pdfincr% --output %pdftarget%

REM Class Number column headers

"C:\Files\VSProjects\Softwords.Pdf.Tools\Softwords.Pdf.Tools\bin\Release\cenoPostProcess.exe" --tool "setcounters" --files %pdftarget% --fieldmask "Room.Class.No.##" --segment 3 --increment 1 --output %pdftarget%

REM Combine
"C:\Files\VSProjects\Softwords.Pdf.Tools\Softwords.Pdf.Tools\bin\Release\cenoPostProcess.exe" -d C:\Files\Pineapples\Projects\Kemis\Form\PRI\out -c "KEMIS 2015 PRI.pdf" -f "KEMIS PRI*.pdf" --nopause

"C:\Files\VSProjects\Softwords.Pdf.Tools\Softwords.Pdf.Tools\bin\Release\cenoPostProcess.exe" -d C:\Files\Pineapples\Projects\Kemis\Form\PRI\out -f "KEMIS 2015 PRI.pdf"  --pwd kiri --title "Kiribati Primary Survey 2015"  -j "C:\Files\Pineapples\Projects\Kemis\Form\js"

