# Pacific EMIS PDF eSurveys #

Pacific EMIS is a web application is active development being led and supported by the Secretariat of the Pacific Community (SPC) and adopted by small Pacific Islands nations. This repository hold the eSurvey technologies used primarily in data collection using PDF that can save data for later processing. 

### Documentation ###

Various documentation for this technology exist for various types of users

* Users can find documentation on the process of how to use surveys to collect data and import into their country EMIS in the general User Guide (TODO link to online doc)
* Systems Administrators can find documentation on how to install the tools for users in the Systems Administrator Guide (TODO link to online doc)
* Developers can find documentation on how to created new surveys and how it all fits in the EMIS system as a whole in the Developer Guide (TODO link to online doc)

### New Countries ###

New adopting countries should create a new directory to hold their source word documents and generated PDF surveys. The same convention as used with kemis, siemis and miemis for Kiritbati, Solomon Islands and Marshall Islands should be used and you can just look at those to see how it works.
 
### Access Rights to this Repository ###

While anybody from adopting countries can view their surveys only people given permission can push new changes to these files

### Who do I talk to? ###

For further information you can contact SPC Pacific EMIS team (TODO include details?!)